package com.example.demo.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.domains.Parameter;
import com.example.demo.domains.River;
import com.example.demo.services.ParameterService;
import com.example.demo.services.RiverService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class RiverController {

	@Autowired
	RiverService riverServ;
	
	@Autowired
	ParameterService parameterServ;
	
	
	@GetMapping("/addRiver")
	public String getAddRiver(Model model) {
		model.addAttribute("river", new River());
		return  "addRiver";		
	}
	@GetMapping("/ShowRivers")
	public String showRiversPage(Model model) {
		List<River> rivers=riverServ.findAllRivers();
		List<Parameter> parameters=parameterServ.findAllParameters();
		log.info("Size: ",rivers.size());
		if(rivers.isEmpty()) 
			model.addAttribute("rivers","empty");
		else
			model.addAttribute("rivers",rivers);
		if(parameters.isEmpty()) 
			model.addAttribute("parameters","empty");
		else
			model.addAttribute("parameters",parameters);
		return "ShowRivers";
	}
	
   @PostMapping(params="add",path="/addRiver")	
   public String addRiver(@ModelAttribute("river") River river,Errors errors,RedirectAttributes attribute,@RequestParam("file")MultipartFile file) {
	   if(errors.hasErrors()) {
			return  "addRiver";		
		}
	   
	   if (!file.getOriginalFilename().isEmpty()) {

		      try {
		        int i = 1;
		        String filename = file.getOriginalFilename();
		        int pos = filename.lastIndexOf(".");
		        String dir = "src/main/resources/static/images";
		        File directory = new File(dir);
		        if (!directory.exists()) {
		          directory.mkdir();
		        }
		        String name = "";
		        if (pos > 0) {
		          name += filename.substring(0, pos);
		        }
		        String absPath = directory.getAbsolutePath();
		        // absPath.replaceAll("\\","/");
		        
		        String filePath = absPath + "/" + name + "/";
		        File destFolder = new File(filePath);

		        while (destFolder.exists()) {
		          filePath = directory.getAbsolutePath() + "/" + name + "(" + i + ")/";
		          destFolder = new File(filePath);
		          i++;
		        }

		        destFolder.mkdir();

		        filePath += filename;
		        
		        File dest = new File(filePath);

		        file.transferTo(dest);

		        river.setResourcePath("images/" + name + "/" + name + ".jpg");

		      }
		      
		catch(FileNotFoundException e) {
			log.info(e.getMessage());
		}
		catch(IOException e) {
			log.info(e.getMessage());
		}
	   }
	   riverServ.saveRiver(river);
	   attribute.addFlashAttribute("success","River has been added successfully");
	   return "redirect:/addRiver";
	   }
   }
	

	  
