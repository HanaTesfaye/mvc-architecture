package com.example.demo.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.domains.Parameter;
import com.example.demo.domains.River;
import com.example.demo.domains.RiverStatus;
import com.example.demo.services.ParameterService;
import com.example.demo.services.RiverService;
import com.example.demo.services.RiverStatusService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/AddStatus")
public class RiverStatusController {
	
	@Autowired
	RiverService riverService;
	
	@Autowired
	ParameterService parameterService;
	
	@Autowired
	RiverStatusService riverStatService;
	
	@GetMapping
	public String getRiverStatPage(Model model) {
		
		List<River> rivers= riverService.findAllRivers();
		List<Parameter> parameters= parameterService.findAllParameters();
		
		if(!rivers.isEmpty())
			model.addAttribute("rivers", rivers);
		else
			model.addAttribute("rivers", "empty");
		
		if(!parameters.isEmpty())
			model.addAttribute("parameters", parameters);
		else
			model.addAttribute("parameters", "empty");
		
		model.addAttribute("riverValue", new RiverStatus());
		
		return "AddStatus";
		
	}
	
	@PostMapping(params="add")
	public String postRiverStat(@RequestParam(value="riverId")Long riverId,@RequestParam(value="parameterId")Long parameterId,@ModelAttribute("riverValue")RiverStatus riverValue) {
		
		
		riverValue.setRiver(riverService.findRiverById(riverId));
		riverValue.setValues(parameterService.findParamById(parameterId));
		
		riverStatService.saveStatus(riverValue);
		
		return "redirect:/AddStatus";
		
	}

}
