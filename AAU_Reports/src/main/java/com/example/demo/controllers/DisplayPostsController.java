package com.example.demo.controllers;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.domains.Report;
import com.example.demo.services.PostService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class DisplayPostsController {
	@Autowired
	PostService postService;
	

	//TeacherAnnouncementService teacherService;
	
	@GetMapping("/displayPosts")
	public String displayPosts(Model model) {
		
			//List<TeacherAnnouncement> announcementList=teacherService.findPostedAnnouncements(year+"","1");
			List<Report> resourceList=postService.findPost();
			
			
			if(!resourceList.isEmpty()) {
				model.addAttribute("resources",resourceList);
			}
			else {
				model.addAttribute("resources","empty");
			}
			
			
		return "displayPosts";
	}
	
	
	@GetMapping(path="/uploads/**",produces = MediaType.APPLICATION_PDF_VALUE)
	public @ResponseBody byte[] viewUpload(HttpServletRequest request) throws IOException {
		byte arr[]= {};
		
		String path=request.getRequestURI().substring(8);
		
		
		log.info("Path: "+path);
		
	   	String pathDecoded=URLDecoder.decode(path, "UTF-8");
	
	
	     log.info(pathDecoded);
		 InputStream in = new FileInputStream(pathDecoded);
		 arr=in.readAllBytes();
		 in.close();
	     return arr;
	     
	}
}
	