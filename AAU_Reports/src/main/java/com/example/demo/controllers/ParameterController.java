package com.example.demo.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.domains.Parameter;
import com.example.demo.domains.River;
import com.example.demo.services.ParameterService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/addParameter")
public class ParameterController {
	
	@Autowired
	ParameterService parameterServ;
	
	
	@GetMapping
	public String getPage(Model model) {
		model.addAttribute("parameter",new Parameter());
		return "addParameter";
	}
	
	@PostMapping
	public String addParameter(@ModelAttribute("parameter")Parameter p, Errors error, RedirectAttributes attribute) {
		if(error.hasErrors()) {
			return "addParameter";
		}
		parameterServ.saveParameter(p);
		attribute.addFlashAttribute("success", "Parameter has been added successfully");
		return "redirect:/addParameter";
		}
		
	
	


}
