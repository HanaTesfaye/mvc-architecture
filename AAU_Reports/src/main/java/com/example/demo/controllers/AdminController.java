package com.example.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/adminIndex")
public class AdminController {
	@GetMapping
	public String getAdminPage(Model model) {
		return "adminIndex";
		
	}

}
