package com.example.demo.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.domains.RiverStatus;
import com.example.demo.services.ParameterService;
import com.example.demo.services.RiverService;
import com.example.demo.services.RiverStatusService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/drawGraph")
public class GraphController {
	@Autowired
	RiverStatusService riverStatService;
	
	@Autowired
	RiverService riverService;
	
	@Autowired
	ParameterService parameterService;
	
	
	@GetMapping
	public String getDrawGraphPage(Model model,@RequestParam("id")Long riverId,@RequestParam(value="param")Long parameterId) {
		List<RiverStatus> riverStats=riverStatService.findRiverStatusByRiverId(riverId,parameterId);
		int data[]=new int[12] ;
		Arrays.fill(data, 0);
		for(RiverStatus stat:riverStats) {
			switch(stat.getMonth()) {
				case("January"):
					data[0]=stat.getData();
					break;
				case("February"):
					data[1]=stat.getData();
					break;
				case("March"):
					data[2]=stat.getData();
					break;
				case("April"):
					data[3]=stat.getData();
					break;
				case("May"):
					data[4]=stat.getData();
					break;
				case("June"):
					data[5]=stat.getData();
					break;
				case("July"):
					data[6]=stat.getData();
					break;
				case("August"):
					data[7]=stat.getData();
					break;
				case("September"):
					data[8]=stat.getData();
					break;
				case("October"):
					data[9]=stat.getData();
					break;
				case("November"):
					data[10]=stat.getData();
					break;
				case("December"):
					data[11]=stat.getData();
					break;
					
				}
		}
		model.addAttribute("name",riverService.findRiverById(riverId).getName());
		model.addAttribute("parameter",parameterService.findParamById(parameterId).getName());
		model.addAttribute("unit",parameterService.findParamById(parameterId).getUnit());
		model.addAttribute("def", parameterService.findParamById(parameterId).getDefinition());
		model.addAttribute("data", data);
		model.addAttribute("image",riverService.findRiverById(riverId));
		return "/drawGraph";
	}
	
	
	
}
