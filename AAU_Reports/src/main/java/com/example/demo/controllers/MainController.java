package com.example.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.example.demo.domains.River;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Controller
public class MainController {
	@GetMapping("/")	
	public String getHome(Model model) {
		
		return  "index";		
	}

}
