package com.example.demo.repositories;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.domains.Report;

public interface PostRepository extends CrudRepository<Report, Long> {
	
	@Query(value="SELECT * FROM reports where reports.report_path IS NOT NULL",nativeQuery=true)
	List<Report> findAll();
	
	
/*	@Query(value="SELECT * FROM resources where posts.resource_path IS NULL AND posts.year=:choice AND posts.semester=:semester",nativeQuery=true)
	List<Resource> findPostedAnnouncement(String choice,String semester);
	
	@Query(value="SELECT * FROM posts where posts.resource_path IS NULL AND posts.teacher_id=:id",nativeQuery=true)
	List<Resource> findPostedAnnouncementsByUser(Long id);
	
	@Query(value="SELECT * FROM resources where resources.resource_path IS NOT NULL AND resources.teacher_id=:id",nativeQuery=true)
	List<Resource> findPostedResourcesByUser(Long id);*/
}
