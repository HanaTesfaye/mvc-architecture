package com.example.demo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.domains.RiverStatus;

public interface RiverStatusRepository extends CrudRepository<RiverStatus, Long> {
	
	@Query(value="SELECT * FROM river_values where river_values.river_id=:rid AND river_values.parameter_id=:pid",nativeQuery=true)
	 List<RiverStatus> findStatusByRiverAndParameterId(Long rid,Long pid);
}
