package com.example.demo.repositories;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.domains.Parameter;

public interface ParameterRepository extends CrudRepository<Parameter, Long> {

}
