package com.example.demo.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.domains.Story;

public interface StoryRepository extends CrudRepository<Story,Long>{
	Page<Story> findAll(Pageable pageRequest);
	Page<Story> findByType(Pageable pageRequest,String type);
	int countByType(String type);
}
