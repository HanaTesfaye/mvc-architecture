package com.example.demo.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.domains.River;

public interface RiverRepository extends CrudRepository<River, Long> {

}
