package com.example.demo.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.domains.Comment;

public interface CommentRepository extends CrudRepository<Comment,Long> {
	Page<Comment> findByOnWhat(Pageable pageble,long onWhat);
	
}
