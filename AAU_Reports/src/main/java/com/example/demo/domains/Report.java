package com.example.demo.domains;

import java.io.File;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "reports")
public class Report {
    
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="created_at")
	private Date createdAt;
	
	@PrePersist
	void placedAt() {
		this.createdAt = new Date();
	}	
	
	@NotEmpty(message="Title cannot be empty")
	@NotNull
    private String title;
    
    @Column(name = "description")
    @NotEmpty(message = "Please provide the description")
    @NotNull
    private String description;
  
    @Column(name = "report_path")
	private String reportPath=null;
  
	@Transient
	private File reportFile;
	
}
