package com.example.demo.domains;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


import lombok.Data;

@Data
@Entity
@Table(name = "parameter")
public class Parameter {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "definition")
	@NotEmpty(message="Definition cannot be empty")
	@NotNull
    private String definition;
    
	
	@Column(name = "name")
    @NotEmpty(message = "Please provide the name")
    @NotNull
    private String name;
	

	@NotEmpty(message="Unit cannot be empty")
	@NotNull
    private String unit;
  
	
}
