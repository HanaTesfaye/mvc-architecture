package com.example.demo.domains;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "river_values")
public class RiverStatus {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="created_at")
	private Date createdAt;
	
	@PrePersist
	void placedAt() {
		this.createdAt = new Date();
	}	
    
    @Column(name = "data")
    @NotNull(message = "Please provide the data")
    private int data;
    
    @Column(name = "month")
    @NotEmpty(message = "Please provide the month")
    @NotNull
    private String month;
    
    @ManyToOne(fetch=FetchType.LAZY)
   	@JoinColumn(name="parameter_id")
    private Parameter values;
    
    @ManyToOne(fetch=FetchType.LAZY)
   	@JoinColumn(name="river_id")
    private River river;
    
    

	

}
