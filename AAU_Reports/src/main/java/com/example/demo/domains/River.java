package com.example.demo.domains;

import java.io.File;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "river")
public class River {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "name")
	@NotEmpty(message="Name cannot be empty")
	@NotNull
    private String name;
	
	@Column(name = "resource_path")
	private String resourcePath=null;
	
	@Transient
	private File resourceFile;
	
    @Column(name = "locationUp")
    @NotEmpty(message = "Please provide the location")
    @NotNull
    private String locationUp;
    
    @Column(name = "locationDown")
    @NotEmpty(message = "Please provide the location")
    @NotNull
    private String locationDown;

    @Column(name = "locationRight")
    @NotEmpty(message = "Please provide the location")
    @NotNull
    private String locationRight;
    
    @Column(name = "locationLeft")
    @NotEmpty(message = "Please provide the location")
    @NotNull
    private String locationLeft;
    
}
