package com.example.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.domains.Parameter;
import com.example.demo.domains.River;
import com.example.demo.repositories.ParameterRepository;
import com.example.demo.repositories.RiverRepository;

@Service
public class RiverServiceImpl implements RiverService {
	 
	@Autowired
	RiverRepository riverRepo;

	@Override
	public void saveRiver(River r) {
		riverRepo.save(r);
	}

	@Override
	public List<River> findAllRivers() {
		return (List<River>)riverRepo.findAll();
	}

	@Override
	public River findRiverById(Long id) {
		return riverRepo.findById(id).get();
	}

	
	
}
