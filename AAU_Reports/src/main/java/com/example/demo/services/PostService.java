package com.example.demo.services;

import java.util.List;
import java.util.Optional;

import com.example.demo.domains.Report;


public interface PostService {
	void savePost(Report r);
	List<Report> findPost();
}
