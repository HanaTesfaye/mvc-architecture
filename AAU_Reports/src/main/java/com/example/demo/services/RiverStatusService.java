package com.example.demo.services;

import java.util.List;

import com.example.demo.domains.RiverStatus;

public interface RiverStatusService {
	void saveStatus(RiverStatus r);
	List<RiverStatus> findAll();
	List<RiverStatus> findRiverStatusByRiverId(Long rid,Long pid);

}
