package com.example.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.domains.RiverStatus;
import com.example.demo.repositories.RiverStatusRepository;

@Service
public class RiverStatusServiceImpl implements RiverStatusService {
	
	@Autowired
	RiverStatusRepository riverStatRepo;

	@Override
	public void saveStatus(RiverStatus r) {
		riverStatRepo.save(r);

	}

	@Override
	public List<RiverStatus> findAll() {
		
		return (List<RiverStatus>)riverStatRepo.findAll();
	}

	@Override
	public List<RiverStatus> findRiverStatusByRiverId(Long rid,Long pid) {
		
		return (List<RiverStatus>)riverStatRepo.findStatusByRiverAndParameterId(rid,pid);
	}

	

}
