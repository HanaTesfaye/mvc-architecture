package com.example.demo.services;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.domains.Report;
//import com.web.NoticeBoard.domains.User;
import com.example.demo.repositories.PostRepository;
//import com.web.NoticeBoard.domains.Resource;

@Service
public class PostServiceImpl implements PostService{
	
	@Autowired
	PostRepository postRepository;
	

	@Override
	public void savePost(Report post) {
		postRepository.save(post);
	}


	@Override
	public List<Report> findPost() {
		// TODO Auto-generated method stub
		List<Report> posts =postRepository.findAll();
		return posts;
		
	}
}
