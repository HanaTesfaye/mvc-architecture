package com.example.demo.services;

import java.util.List;

import com.example.demo.domains.Parameter;

public interface ParameterService {
	void saveParameter(Parameter p);
	List<Parameter> findAllParameters();
	Parameter findParamById(Long id);

}
