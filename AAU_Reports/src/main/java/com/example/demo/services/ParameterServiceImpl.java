package com.example.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.domains.Parameter;
import com.example.demo.repositories.ParameterRepository;

@Service
public class ParameterServiceImpl implements ParameterService {
	@Autowired
	ParameterRepository parameterRepo;

	@Override
	public void saveParameter(Parameter p) {
		parameterRepo.save(p);
	}

	@Override
	public List<Parameter> findAllParameters() {
		return (List<Parameter>)parameterRepo.findAll();
	}

	@Override
	public Parameter findParamById(Long id) {
		return parameterRepo.findById(id).get();
	}

}
