package com.example.demo.services;

import java.util.List;

import com.example.demo.domains.Parameter;
import com.example.demo.domains.River;

public interface RiverService {
	void saveRiver(River r);
	List<River> findAllRivers();
	River findRiverById(Long id);
}
